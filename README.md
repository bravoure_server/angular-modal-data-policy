# Bravoure - Data Policy Modal

This is a modal window component to help comply to the [GDPR](https://www.eugdpr.org/) General Data Protection Regulation.

It won't handle any data submission, you have to do that yourself, for every form.

## Install

Add the package to the bower.json file in `extend/`:

```js
{
  // ...
  "dependencies": {
    // ...
    "angular-modal-data-policy": "^1.0.0"
  }
}
```

then run `bower install` in the terminal.

### Dependencies

This module expects the following libraries to be globally available:

- [Swiper](http://idangero.us/swiper/) 3.x

Install them in `extend/bower` if not already present in the project.

## Usage

Make sure the following strings are available in the translation files:

```js
"modal_data_policy.back": "Back",
"modal_data_policy.data_policy": "Data Policy",
"modal_data_policy.communications": "Communications",
"modal_data_policy.required": "Required",
"modal_data_policy.continue": "Continue",
"modal_data_policy.agree": "I agree",
"modal_data_policy.wait": "Please wait…",
"modal_data_policy.why": "Why am I seeing this?",
"modal_data_policy.why_url": "#TODO" // the URL for the "Why am I seeing this?" link
```

Import the SCSS in `main_extend.scss`.

```scss
@import 'jello_components/angular-modal-data-policy/modal-data-policy';
```

Add the directive somewhere up the tree, `extend-index.html` is a good place.

```html
<modal-data-policy></modal-data-policy>
```

Then in the form directive, once the user clicks to submit and you know the form is valid, emit a `dataPolicy:check` event on the `$rootScope`, passing in an object (more on this below).

```js
// This will open the modal
$rootScope.$emit('dataPolicy:check', policyCheck);
```

The `policyCheck` object should have 1 property and 2 methods:

- `categoryTitle` _(string)_ This should match a `gdpr_category` in the CMS. There can be many policies for different forms on the same website. This lets you pick only the data needed for each form.
- `agree(data[, done])` Get the data policies back from the modal. You then can merge that into your form and submit it. The optional second argument `done` is a callback that lets you close the modal yourself; if you need to do any async work for the form submission (like an API call), use it.
- `cancel()` No data back since the user didn't agree to the terms. Lets you do any clean-up.

```js
var policyCheck = {
    categoryTitle: 'newsletter',

    agree: function(data) {
        // Get `data` back and handle the form submission..
    },

    cancel: function() {
        // The user didn't agree, or just closed the modal
        // so do anything you need to do to clear/reset the form
    }
};
```

`data` is an array of "policy" objects with the following signature:

```
{
  id: number, // matches the `id` in the CMS
  text: string,
  required: bool,
  accepted: bool
}
```

### GDPR Service

This component also creates an Angular service called `GDPR`. Inject it in your directives as you do with other Angular modules. It currently has only 1 method and 1 property, both useful in case you need to submit the GDPR to Core:

- **`createDataObject(category, data)`** Returns an object with the signature needed in Core to store the GDPR data. `category` should match `policyCheck.categoryTitle`. `data` is the data you will be getting in the `policyCheck.agree()` callback.
- **`SUBMISSION_URL`** The URL in Core to submit the data to (/api/data-policy/submission?lang={lang}). 

## Styles

### Colors

You can set the following variables in `_variables.scss` (_before_ importing the component styles), to change the colors of the modal:

```scss
$modal-data-policy-backdrop-color: black;
$modal-data-policy-background-color: white;
$modal-data-policy-text-color: black;
$modal-data-policy-primary-color: black;
$modal-data-policy-secondary-color: rgb(150, 150, 150);
```

### Other styles

If you'd like to change fonts, please do so by adding your rules to classes like `.modal-data-policy__body-title`, `.modal-data-policy__accordion-label`, etc. Remember to do it _after_ importing the component styles).