(function() {
    'use strict';

    /*
        Data Policy Modal
        -----------------

        Dependencies:
        - Swiper 3.x

        TODO
        ----
        - [ ] Decide what to do if no data comes from the CMS
    */

    function modalDataPolicy(
        PATH_CONFIG,
        $rootScope,
        $timeout,
        Restangular,
        $translate,
        $log
    ) {
        return {
            restrict: 'E',

            link: function(scope, element) {
                var $el = $(element);

                // Accordion
                $el.on('click', '.js-accordion-btn', function(event) {
                    var $this = $(this);
                    var $item = $this.parent();
                    var $content = $this.next();

                    // Toggle .open class on accordion items
                    $item
                        .toggleClass('open')
                        .siblings()
                        .removeClass('open');

                    // Expand or collapse this panel
                    $content.slideToggle('fast');

                    // Hide the other panels
                    $item
                        .closest('.js-accordion-wrap')
                        .find('.js-accordion-content')
                        .not($content)
                        .slideUp('fast');
                });
            },

            controller: function($scope, $controller) {
                // Extend controller
                angular.extend(
                    this,
                    $controller('baseController', {
                        $scope: $scope
                    })
                );

                var pathname = 'api/library/lists/data_policy_modals';

                // URL to get data from the CMS
                var URL =
                    host + pathname + '?lang=' + PATH_CONFIG.current_language;

                // Time we want to wait before closing the modal,
                // if receiver forgets to call the close callback
                var AFTER_SUBMIT_TIMEOUT = 5000;
                var ESC = 27;
                var IS_OPEN = 'is-modal-data-policy-open';

                var $window = $(window);
                var $body = $(document.body);
                var swipers = {
                    header: null,
                    content: null
                };

                /*
                    This is how the modal gets opened from outside,
                    it listens to the `dataPolicy:check` event,
                    which takes a `check` object to talk back..

                    `check` is an object that should contain 3 props:
                    - `categoryTitle` {string} to match the data coming from the CMS
                    - `agree` {function} to send the policy data back to the form
                    - `cancel` {function} to send nothing and avoid submitting  
                */
                $rootScope.$on('dataPolicy:check', function onDataPolicyCheck(
                    event,
                    check
                ) {
                    fetchData(check.categoryTitle).then(function dataFetched(
                        data
                    ) {
                        if (data === null) {
                            $log.warn(
                                'modal-data-policy: The modal could not be opened ' +
                                    'because no data was found with category `' +
                                    check.categoryTitle +
                                    '`'
                            );
                            return check.cancel(); // TODO is this the right thing to do?
                        }
                        data.content.communications = transformCommunications(
                            data.content.communications
                        );
                        $scope.content = angular.copy(data.content);
                        $scope.check = check;
                        open();
                    });
                });

                $scope.isOpen = false;
                $scope.activeIndex = 0;
                $scope.isSubmitting = false;
                $scope.isSubmittable = isSubmittable;
                $scope.buttonValue = buttonValue;

                $scope.cancel = cancel;
                $scope.back = back;
                $scope.next = next;

                // Computed value to enable/disable the submit button
                function isSubmittable() {
                    if ($scope.isSubmitting === true) {
                        return false;
                    }
                    if ($scope.activeIndex < 1) {
                        return true;
                    }

                    return $scope.content.communications.every(
                        validityCallback
                    );
                }

                // Computed value for the submit button text
                function buttonValue() {
                    if ($scope.isSubmitting === true) {
                        return $translate.instant('modal_data_policy.wait');
                    }
                    if ($scope.activeIndex < 1) {
                        return $translate.instant('modal_data_policy.agree');
                    }

                    return $translate.instant('modal_data_policy.continue');
                }

                // If any `required` item is not `accepted`,
                // this will return `false`
                function validityCallback(x, index) {
                    if (x.required !== true) {
                        return true;
                    }

                    return x.accepted;
                }

                function submit() {
                    var data = angular.copy($scope.content.communications);
                    var timeout;

                    $scope.isSubmitting = true;

                    // Warn if `agree()` is not taking any arguments
                    if ($scope.check.agree.length === 0) {
                        $log.warn(
                            'modal-data-policy: The check.agree() callback is not taking any arguments'
                        );
                    }

                    // If `agree` takes 2 arguments instead of 1, pass the `closeCallback`
                    // so the receiver can close the modal themselves after doing their thing
                    if ($scope.check.agree.length === 2) {
                        timeout = setTimeout(close, AFTER_SUBMIT_TIMEOUT);
                        $scope.check.agree(data, closeCallback);
                        return;
                    }

                    $scope.check.agree(data);
                    close();

                    function closeCallback() {
                        if (timeout != null) {
                            clearTimeout(timeout);
                            timeout = null;
                        }
                        close();
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                }

                function cancel() {
                    if ($scope.check != null) {
                        $scope.check.cancel();
                        $scope.check = null;
                    }
                    close();
                }

                function back() {
                    if (swipers.content) {
                        swipers.content.slidePrev();
                    }
                }

                function next() {
                    switch ($scope.activeIndex) {
                        case 0:
                            swipers.content && swipers.content.slideNext();
                            break;
                        default:
                            submit();
                            break;
                    }
                }

                // Open the modal
                function open() {
                    $scope.isOpen = true;
                    $scope.activeIndex = 0;
                    $body.addClass(IS_OPEN);
                    $window.on('keydown', onKeydown);
                    $timeout(createSwipers);
                }

                // Bind `activeIndex` to $scope
                function onSlideChangeEnd(swiper) {
                    $timeout(function() {
                        $scope.activeIndex = swiper.activeIndex;
                    });
                }

                // Create swipers
                function createSwipers() {
                    swipers.header = new Swiper(
                        '.modal-data-policy__swiper-header',
                        {
                            effect: 'fade',
                            noSwiping: true,
                            simulateTouch: false,
                            allowTouchMove: false,
                            draggable: false,
                            touchAngle: 0,
                            touchRatio: 0
                        }
                    );

                    swipers.content = new Swiper(
                        '.modal-data-policy__swiper-content',
                        {
                            noSwiping: true,
                            simulateTouch: false,
                            allowTouchMove: false,
                            draggable: false,
                            touchAngle: 0,
                            touchRatio: 0,
                            pagination: '.swiper-pagination-custom',
                            control: swipers.header, // link control
                            onSlideChangeEnd: onSlideChangeEnd
                        }
                    );
                }

                // Close modal with the ESC key
                function onKeydown(event) {
                    if (event.keyCode === ESC && $scope.isOpen) {
                        cancel();
                    }
                }

                // Close the modal
                function close() {
                    if (swipers.header && swipers.content) {
                        swipers.header.destroy();
                        swipers.content.destroy();
                    }
                    $window.off('keydown', onKeydown);
                    $body.removeClass(IS_OPEN);
                    $scope.isSubmitting = false;
                    $scope.content = null;
                    $scope.isOpen = false;
                }

                // Get data from the CMS
                function fetchData(categoryTitle) {
                    return Restangular.allUrl('modals', URL)
                        .getList()
                        .then(function(modals) {
                            if (modals.plain().length === 0) {
                                $log.error(
                                    'modal-data-policy: No data was returned from the CMS at endpoint ' +
                                        pathname
                                );
                            }
                            var values = modals.plain().filter(categoryFilter);
                            return values.length === 0 ? null : values[0];
                        });

                    function categoryFilter(item) {
                        return (
                            item.content.gdpr_category[0].title ===
                            categoryTitle
                        );
                    }
                }

                // Transform `communications` into something more manageable
                function transformCommunications(communications) {
                    return communications.reduce(callback, []);

                    function callback(result, item) {
                        result.push({
                            id: item.content.id,
                            text: item.content.data_policy_item_text,
                            required: item.content.required === 1, // make bool for consistency
                            accepted: false
                        });

                        return result;
                    }
                }
            },

            templateUrl:
                PATH_CONFIG.BRAVOURE_COMPONENTS +
                'angular-modal-data-policy/modal-data-policy.html'
        };
    }

    modalDataPolicy.$inject = [
        'PATH_CONFIG',
        '$rootScope',
        '$timeout',
        'Restangular',
        '$translate',
        '$log'
    ];

    /* 
        This factory returns an object with helpers:
        - `createDataObject()`
        - `SUBMISSION_URL`
    */
    function GDPRFactory(PATH_CONFIG) {
        // URL to submit the GDPR data in Core
        var SUBMISSION_URL =
            host +
            'api/data-policy/submission?lang=' +
            PATH_CONFIG.current_language;

        /**
         * This creates an object with the signature
         * needed in Core to store GDPR data
         * https://bravourebv.atlassian.net/wiki/spaces/CDS/pages/202866689/GDPR+setup+on+Standard+products
         * 
         * @param {string} category The `categoryTitle` used in `policyCheck`
         * @param {Array} data The `data` coming back from the `agree()` callback
         * @returns {object}
         */
        function createDataObject(category, data) {
            if (!angular.isString(category)) {
                throw new TypeError(
                    'GDPR.createDataObject(category, data): `category` must be a string'
                );
            }
            if (!angular.isArray(data)) {
                throw new TypeError(
                    'GDPR.createDataObject(category, data): `data` must be an array,' +
                        'it expects the `data` from the GDPR modal `agree` callback'
                );
            }

            var items = data
                .filter(function(x) {
                    return x.accepted;
                })
                .map(function(x) {
                    return {
                        id: x.id,
                        text: x.text
                    };
                });

            return {
                user_id: null,
                data_policy_group: category,
                data_policy_accepted: 1,
                data_policy_accepted_items: items
            };
        }

        return {
            createDataObject: createDataObject,
            SUBMISSION_URL: SUBMISSION_URL
        };
    }

    angular
        .module('bravoureAngularApp')
        .directive('modalDataPolicy', modalDataPolicy)
        .factory('GDPR', GDPRFactory);
})();
